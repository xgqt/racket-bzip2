#lang info

(define collection 'multi)

(define name "bzip2")
(define version "0.1")

(define deps
  '("base"
    "rackunit-lib"))

(define build-deps
  '("racket-doc"
    "scribble-lib"))
